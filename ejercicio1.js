var vector = [1, 'dos', true, 4.5, false, 'seis'];

console.log(vector);

console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

vector[2] = 'tres';

console.log('Longitud del vector:', vector.length);

vector.push(7);

var ultimoElemento = vector.pop();
console.log('Último elemento eliminado:', ultimoElemento);

vector.splice(3, 0, 'nuevoElemento');

var primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

vector.unshift(primerElemento);

console.log('Vector final:', vector);
