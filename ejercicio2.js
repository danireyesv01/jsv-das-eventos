var vector = [1, 'dos', true, 4.5, false, 'seis'];

console.log('Usando "for":');
for (var i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

console.log('\nUsando "forEach":');
vector.forEach(function (valor) {
  console.log(valor);
});

console.log('\nUsando "map":');
vector.map(function (valor) {
  console.log(valor);
});

console.log('\nUsando "while":');
var i = 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}

console.log('\nUsando "for..of":');
for (var valor of vector) {
  console.log(valor);
}
